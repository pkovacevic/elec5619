package com.comp5615.web;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.comp5615.domain.Users;

@RequestMapping("/signup")
@Controller
@RooWebScaffold(path = "signup", formBackingObject = Users.class)
public class signupController {

}
