// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.comp5615.web;

import com.comp5615.domain.Review;
import com.comp5615.domain.Users;
import com.comp5615.web.ReviewController;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect ReviewController_Roo_Controller {
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String ReviewController.create(@Valid Review review, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, review);
            return "reviews/create";
        }
        uiModel.asMap().clear();
        review.persist();
        return "redirect:/reviews/" + encodeUrlPathSegment(review.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String ReviewController.createForm(Model uiModel) {
        populateEditForm(uiModel, new Review());
        return "reviews/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String ReviewController.show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("review", Review.findReview(id));
        uiModel.addAttribute("itemId", id);
        return "reviews/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String ReviewController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("reviews", Review.findReviewEntries(firstResult, sizeNo));
            float nrOfPages = (float) Review.countReviews() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("reviews", Review.findAllReviews());
        }
        addDateTimeFormatPatterns(uiModel);
        return "reviews/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String ReviewController.update(@Valid Review review, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, review);
            return "reviews/update";
        }
        uiModel.asMap().clear();
        review.merge();
        return "redirect:/reviews/" + encodeUrlPathSegment(review.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String ReviewController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Review.findReview(id));
        return "reviews/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String ReviewController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Review review = Review.findReview(id);
        review.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/reviews";
    }
    
    void ReviewController.addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("review_reviewdate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }
    
    void ReviewController.populateEditForm(Model uiModel, Review review) {
        uiModel.addAttribute("review", review);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("userses", Users.findAllUserses());
    }
    
    String ReviewController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
