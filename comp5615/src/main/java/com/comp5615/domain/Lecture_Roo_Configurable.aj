// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.comp5615.domain;

import com.comp5615.domain.Lecture;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect Lecture_Roo_Configurable {
    
    declare @type: Lecture: @Configurable;
    
}
