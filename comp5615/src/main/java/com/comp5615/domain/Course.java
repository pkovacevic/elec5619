package com.comp5615.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findCoursesByTags" })
public class Course {

    @NotNull
    @Size(min = 2)
    private String name;

    @Size(min = 2)
    private String description;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Users> subscribedUsers = new HashSet<Users>();

    private ArrayList<String> tags = new ArrayList<String>();

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Review> reviews = new HashSet<Review>();
}
