// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.comp5615.domain;

import com.comp5615.domain.Course;
import com.comp5615.domain.Review;
import com.comp5615.domain.Users;
import java.util.ArrayList;
import java.util.Set;

privileged aspect Course_Roo_JavaBean {
    
    public String Course.getName() {
        return this.name;
    }
    
    public void Course.setName(String name) {
        this.name = name;
    }
    
    public String Course.getDescription() {
        return this.description;
    }
    
    public void Course.setDescription(String description) {
        this.description = description;
    }
    
    public Set<Users> Course.getSubscribedUsers() {
        return this.subscribedUsers;
    }
    
    public void Course.setSubscribedUsers(Set<Users> subscribedUsers) {
        this.subscribedUsers = subscribedUsers;
    }
    
    public ArrayList<String> Course.getTags() {
        return this.tags;
    }
    
    public void Course.setTags(ArrayList<String> tags) {
        this.tags = tags;
    }
    
    public Set<Review> Course.getReviews() {
        return this.reviews;
    }
    
    public void Course.setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }
    
}
