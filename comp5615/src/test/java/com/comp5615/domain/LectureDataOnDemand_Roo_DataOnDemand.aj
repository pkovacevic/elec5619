// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.comp5615.domain;

import com.comp5615.domain.Lecture;
import com.comp5615.domain.LectureDataOnDemand;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.stereotype.Component;

privileged aspect LectureDataOnDemand_Roo_DataOnDemand {
    
    declare @type: LectureDataOnDemand: @Component;
    
    private Random LectureDataOnDemand.rnd = new SecureRandom();
    
    private List<Lecture> LectureDataOnDemand.data;
    
    public Lecture LectureDataOnDemand.getNewTransientLecture(int index) {
        Lecture obj = new Lecture();
        return obj;
    }
    
    public Lecture LectureDataOnDemand.getSpecificLecture(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        Lecture obj = data.get(index);
        Long id = obj.getId();
        return Lecture.findLecture(id);
    }
    
    public Lecture LectureDataOnDemand.getRandomLecture() {
        init();
        Lecture obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return Lecture.findLecture(id);
    }
    
    public boolean LectureDataOnDemand.modifyLecture(Lecture obj) {
        return false;
    }
    
    public void LectureDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = Lecture.findLectureEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'Lecture' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<Lecture>();
        for (int i = 0; i < 10; i++) {
            Lecture obj = getNewTransientLecture(i);
            try {
                obj.persist();
            } catch (ConstraintViolationException e) {
                StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getConstraintDescriptor()).append(":").append(cv.getMessage()).append("=").append(cv.getInvalidValue()).append("]");
                }
                throw new RuntimeException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
