// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.comp5615.domain;

import com.comp5615.domain.Users;
import com.comp5615.domain.UsersDataOnDemand;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.stereotype.Component;

privileged aspect UsersDataOnDemand_Roo_DataOnDemand {
    
    declare @type: UsersDataOnDemand: @Component;
    
    private Random UsersDataOnDemand.rnd = new SecureRandom();
    
    private List<Users> UsersDataOnDemand.data;
    
    public Users UsersDataOnDemand.getNewTransientUsers(int index) {
        Users obj = new Users();
        setDescription(obj, index);
        setEmail(obj, index);
        setName(obj, index);
        return obj;
    }
    
    public void UsersDataOnDemand.setDescription(Users obj, int index) {
        String description = "description_" + index;
        obj.setDescription(description);
    }
    
    public void UsersDataOnDemand.setEmail(Users obj, int index) {
        String email = "foo" + index + "@bar.com";
        obj.setEmail(email);
    }
    
    public void UsersDataOnDemand.setName(Users obj, int index) {
        String name = "name_" + index;
        obj.setName(name);
    }
    
    public Users UsersDataOnDemand.getSpecificUsers(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        Users obj = data.get(index);
        Long id = obj.getId();
        return Users.findUsers(id);
    }
    
    public Users UsersDataOnDemand.getRandomUsers() {
        init();
        Users obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return Users.findUsers(id);
    }
    
    public boolean UsersDataOnDemand.modifyUsers(Users obj) {
        return false;
    }
    
    public void UsersDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = Users.findUsersEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'Users' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<Users>();
        for (int i = 0; i < 10; i++) {
            Users obj = getNewTransientUsers(i);
            try {
                obj.persist();
            } catch (ConstraintViolationException e) {
                StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getConstraintDescriptor()).append(":").append(cv.getMessage()).append("=").append(cv.getInvalidValue()).append("]");
                }
                throw new RuntimeException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
