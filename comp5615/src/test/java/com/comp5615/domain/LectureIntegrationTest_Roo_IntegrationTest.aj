// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.comp5615.domain;

import com.comp5615.domain.Lecture;
import com.comp5615.domain.LectureDataOnDemand;
import com.comp5615.domain.LectureIntegrationTest;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

privileged aspect LectureIntegrationTest_Roo_IntegrationTest {
    
    declare @type: LectureIntegrationTest: @RunWith(SpringJUnit4ClassRunner.class);
    
    declare @type: LectureIntegrationTest: @ContextConfiguration(locations = "classpath:/META-INF/spring/applicationContext*.xml");
    
    declare @type: LectureIntegrationTest: @Transactional;
    
    @Autowired
    private LectureDataOnDemand LectureIntegrationTest.dod;
    
    @Test
    public void LectureIntegrationTest.testCountLectures() {
        Assert.assertNotNull("Data on demand for 'Lecture' failed to initialize correctly", dod.getRandomLecture());
        long count = Lecture.countLectures();
        Assert.assertTrue("Counter for 'Lecture' incorrectly reported there were no entries", count > 0);
    }
    
    @Test
    public void LectureIntegrationTest.testFindLecture() {
        Lecture obj = dod.getRandomLecture();
        Assert.assertNotNull("Data on demand for 'Lecture' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Lecture' failed to provide an identifier", id);
        obj = Lecture.findLecture(id);
        Assert.assertNotNull("Find method for 'Lecture' illegally returned null for id '" + id + "'", obj);
        Assert.assertEquals("Find method for 'Lecture' returned the incorrect identifier", id, obj.getId());
    }
    
    @Test
    public void LectureIntegrationTest.testFindAllLectures() {
        Assert.assertNotNull("Data on demand for 'Lecture' failed to initialize correctly", dod.getRandomLecture());
        long count = Lecture.countLectures();
        Assert.assertTrue("Too expensive to perform a find all test for 'Lecture', as there are " + count + " entries; set the findAllMaximum to exceed this value or set findAll=false on the integration test annotation to disable the test", count < 250);
        List<Lecture> result = Lecture.findAllLectures();
        Assert.assertNotNull("Find all method for 'Lecture' illegally returned null", result);
        Assert.assertTrue("Find all method for 'Lecture' failed to return any data", result.size() > 0);
    }
    
    @Test
    public void LectureIntegrationTest.testFindLectureEntries() {
        Assert.assertNotNull("Data on demand for 'Lecture' failed to initialize correctly", dod.getRandomLecture());
        long count = Lecture.countLectures();
        if (count > 20) count = 20;
        int firstResult = 0;
        int maxResults = (int) count;
        List<Lecture> result = Lecture.findLectureEntries(firstResult, maxResults);
        Assert.assertNotNull("Find entries method for 'Lecture' illegally returned null", result);
        Assert.assertEquals("Find entries method for 'Lecture' returned an incorrect number of entries", count, result.size());
    }
    
    @Test
    public void LectureIntegrationTest.testFlush() {
        Lecture obj = dod.getRandomLecture();
        Assert.assertNotNull("Data on demand for 'Lecture' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Lecture' failed to provide an identifier", id);
        obj = Lecture.findLecture(id);
        Assert.assertNotNull("Find method for 'Lecture' illegally returned null for id '" + id + "'", obj);
        boolean modified =  dod.modifyLecture(obj);
        Integer currentVersion = obj.getVersion();
        obj.flush();
        Assert.assertTrue("Version for 'Lecture' failed to increment on flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void LectureIntegrationTest.testMergeUpdate() {
        Lecture obj = dod.getRandomLecture();
        Assert.assertNotNull("Data on demand for 'Lecture' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Lecture' failed to provide an identifier", id);
        obj = Lecture.findLecture(id);
        boolean modified =  dod.modifyLecture(obj);
        Integer currentVersion = obj.getVersion();
        Lecture merged = obj.merge();
        obj.flush();
        Assert.assertEquals("Identifier of merged object not the same as identifier of original object", merged.getId(), id);
        Assert.assertTrue("Version for 'Lecture' failed to increment on merge and flush directive", (currentVersion != null && obj.getVersion() > currentVersion) || !modified);
    }
    
    @Test
    public void LectureIntegrationTest.testPersist() {
        Assert.assertNotNull("Data on demand for 'Lecture' failed to initialize correctly", dod.getRandomLecture());
        Lecture obj = dod.getNewTransientLecture(Integer.MAX_VALUE);
        Assert.assertNotNull("Data on demand for 'Lecture' failed to provide a new transient entity", obj);
        Assert.assertNull("Expected 'Lecture' identifier to be null", obj.getId());
        obj.persist();
        obj.flush();
        Assert.assertNotNull("Expected 'Lecture' identifier to no longer be null", obj.getId());
    }
    
    @Test
    public void LectureIntegrationTest.testRemove() {
        Lecture obj = dod.getRandomLecture();
        Assert.assertNotNull("Data on demand for 'Lecture' failed to initialize correctly", obj);
        Long id = obj.getId();
        Assert.assertNotNull("Data on demand for 'Lecture' failed to provide an identifier", id);
        obj = Lecture.findLecture(id);
        obj.remove();
        obj.flush();
        Assert.assertNull("Failed to remove 'Lecture' with identifier '" + id + "'", Lecture.findLecture(id));
    }
    
}
